<?php
/**
 * Copyright 2016 Klarna AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Payments
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * Klarna Payments text filter
 */
class Klarna_Payments_Model_Filter
{
    const KLARNA_LOGO_CDN_URL          = 'https://cdn.klarna.com/1.0/shared/image/generic/logo/%s/basic/logo_black.png';
    const KLARNA_PAYNOW_LOGO_CDN_URL   = 'https://cdn.klarna.com/1.0/shared/image/generic/badge/%s/pay_now/standard/pink.svg';
    const KLARNA_PAYLATER_LOGO_CDN_URL = 'https://cdn.klarna.com/1.0/shared/image/generic/badge/%s/pay_later/standard/pink.svg';
    const KLARNA_SLICEIT_LOGO_CDN_URL  = 'https://cdn.klarna.com/1.0/shared/image/generic/badge/%s/slice_it/standard/pink.svg';

    /**
     * Filter string to replace template tags with values
     *
     * @param string $value
     *
     * @return string
     * @throws Exception
     */
    public function filter($value)
    {
        if (preg_match_all(Varien_Filter_Template::CONSTRUCTION_PATTERN, $value, $constructions, PREG_SET_ORDER)) {
            foreach ($constructions as $index => $construction) {
                $method = $construction[1] . 'Directive';
                $replacedValue = $value;
                if (method_exists($this, $method)) {
                    $replacedValue = $this->$method($construction);
                }

                $value = str_replace($construction[0], $replacedValue, $value);
            }
        }

        return $value;
    }

    /**
     * Insert the Klarna Logo with parameters
     *
     * @param array $construction
     *
     * @return string
     */
    public function klarnaDirective($construction)
    {
        return $this->doFilter($construction, self::KLARNA_LOGO_CDN_URL);
    }

    /**
     * Insert the appropriate logo with parameters
     *
     * @param array  $construction
     * @param string $logo
     *
     * @return string
     */
    protected function doFilter($construction, $logo)
    {
        $params = $this->_getIncludeParameters($construction[2]);
        $width = isset($params['width']) ? (int)$params['width'] : 50;
        $style = isset($params['style']) ? $params['style'] : 'display: inline-block; float: none;vertical-align:text-top';
        $locale = Mage::helper('klarna_payments')->getLocale();
        $src = sprintf($logo, $locale) . '?width=' . (2 * $width);

        return sprintf('<img width="%d" style="%s" src="%s" />', $width, $style, $src);
    }

    /**
     * Get parameters from a directive
     *
     * @param string $value
     *
     * @return array
     */
    protected function _getIncludeParameters($value)
    {
        $tokenizer = new Varien_Filter_Template_Tokenizer_Parameter();
        $tokenizer->setString($value);

        return $tokenizer->tokenize();
    }

    /**
     * Insert the Klarna Pay Now Logo with parameters
     *
     * @param array $construction
     *
     * @return string
     */
    public function paynowDirective($construction)
    {
        return $this->doFilter($construction, self::KLARNA_PAYNOW_LOGO_CDN_URL);
    }

    /**
     * Insert the Klarna Pay Later Logo with parameters
     *
     * @param array $construction
     *
     * @return string
     */
    public function paylaterDirective($construction)
    {
        return $this->doFilter($construction, self::KLARNA_PAYLATER_LOGO_CDN_URL);
    }

    /**
     * Insert the Klarna Slice-It Logo with parameters
     *
     * @param array $construction
     *
     * @return string
     */
    public function sliceitDirective($construction)
    {
        return $this->doFilter($construction, self::KLARNA_SLICEIT_LOGO_CDN_URL);
    }
}
