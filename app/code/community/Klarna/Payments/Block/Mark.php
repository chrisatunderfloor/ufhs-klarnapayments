<?php
/**
 * Klarna_Payments_Block_Mark
 *
 * @copyright Copyright © 2017 Klarna Bank AB. All rights reserved.
 * @author    Joe Constant <joe.constant@klarna.com>
 *
 * For the full copyright and license information, please view the NOTICE
 * and LICENSE files that were distributed with this source code.
 */

class Klarna_Payments_Block_Mark extends Mage_Core_Block_Template
{
    protected $method_code;

    /**
     * @param string $method_code
     */
    public function setMethodCode($method_code)
    {
        $this->method_code = $method_code;
    }

    protected function _toHtml()
    {
        return Mage::getModel('klarna_payments/filter')->filter(Mage::getStoreConfig('payment/' . $this->method_code . '/title'));
    }

    /**
     * @return string
     */
    public function getMethodCode()
    {
        $this->method_code;
    }

    protected function _construct()
    {
        $this->setCacheLifetime(null);
        parent::_construct();
    }

}