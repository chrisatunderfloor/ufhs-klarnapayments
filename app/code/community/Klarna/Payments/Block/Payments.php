<?php
/**
 * Copyright 2016 Klarna AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Payments
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * Klarna payments block
 */
class Klarna_Payments_Block_Payments extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        $this->setCacheLifetime(null);
        parent::_construct();
    }

    /**
     * Get Klarna quote details
     *
     * @return Klarna_Payments_Model_Quote|Varien_Object
     */
    public function getKlarnaQuote()
    {
        try {
            return Mage::helper('klarna_payments/checkout')->getKlarnaQuote();
        } catch (Exception $e) {
            Mage::logException($e);
        }

        return new Varien_Object();
    }

    /**
     * Check if an authorization token has been set
     *
     * @return bool
     */
    public function hasAuthorizationToken()
    {
        return (bool)$this->getAuthorizationToken();
    }

    /**
     * Get the authorization token
     *
     * @return string
     */
    public function getAuthorizationToken()
    {
        return $this->getKlarnaQuote()->getAuthorizationToken();
    }

    /**
     * Get client token for checkout session
     *
     * @return string
     */
    public function getClientToken()
    {
        return $this->getKlarnaQuote()->getClientToken();
    }

    /**
     * If the Klarna pre-screen is enabled
     *
     * @return bool
     */
    public function getPreScreenEnabled()
    {
        return Mage::getStoreConfigFlag('payment/klarna_payments/pre_screen');
    }

    public function getFunctionName()
    {
        return 'Credit';
    }

    public function getMethodCode()
    {
        return 'klarna_payments';
    }
}
