<?php
/**
 * Copyright 2016 Klarna AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Payments
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * Klarna payments payment info
 */
class Klarna_Payments_Block_Info_Direktdebit extends Mage_Payment_Block_Info
{
    /**
     * Set template
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('klarnapayments/payment/info.phtml');
    }

    /**
     * Prepare information for payment
     *
     * @param Varien_Object|array $transport
     *
     * @return Varien_Object
     */
    protected function _prepareSpecificInformation($transport = null)
    {
        $transport         = parent::_prepareSpecificInformation($transport);
        $info              = $this->getInfo();
        $klarnaReferenceId = $info->getAdditionalInformation('klarna_reference');
        $order             = $info->getOrder();
        if ($order) {
            $klarnaOrder  = Mage::getModel('klarna_core/order')->loadByOrder($order);
            $isAdminBlock = $this->getParentBlock()
                && $this->getParentBlock() instanceof Mage_Adminhtml_Block_Sales_Order_Payment;

            if ($isAdminBlock && $klarnaOrder->getId() && $klarnaOrder->getSessionId()) {
                $transport->setData($this->helper('klarna_payments')->__('Order ID'), $klarnaOrder->getSessionId());

                if ($klarnaOrder->getReservationId()
                    && $klarnaOrder->getReservationId() != $klarnaOrder->getSessionId()
                ) {
                    $transport->setData(
                        $this->helper('klarna_core')
                        ->__('Reservation'), $klarnaOrder->getReservationId()
                    );
                }
            }

            if ($klarnaReferenceId) {
                $transport->setData($this->helper('klarna_payments')->__('Reference'), $klarnaReferenceId);
            }

            $invoices = $order->getInvoiceCollection();
            foreach ($invoices as $invoice) {
                if ($invoice->getTransactionId()) {
                    $invoiceKey = $this->helper('klarna_payments')->__('Invoice ID (#%s)', $invoice->getIncrementId());
                    $transport->setData($invoiceKey, $invoice->getTransactionId());
                }
            }
        }

        return $transport;
    }
}
