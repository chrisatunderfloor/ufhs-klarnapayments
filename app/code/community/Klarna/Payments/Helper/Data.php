<?php
/**
 * Copyright 2016 Klarna AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Payments
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * Klarna Payments data helper
 */
class Klarna_Payments_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Check if a store allows data sharing
     *
     * @param Mage_Core_Model_Store $store
     *
     * @return bool
     */
    public function getDataSharingEnabled($store = null)
    {
        return Mage::getStoreConfigFlag('payment/klarna_payments/data_sharing', $store);
    }

    public function getLocale($store = null)
    {
        $locale = Mage::getStoreConfig('general/locale/code', $store);
        if (!$locale) {
            return 'en_us';
        }

        return strtolower($locale);
    }
}
