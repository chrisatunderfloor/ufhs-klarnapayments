<?php
/**
 * Copyright 2016 Klarna AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Payments
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * Klarna Payments checkout helper
 */
class Klarna_Payments_Helper_Direktdebit extends Klarna_Payments_Helper_Checkout
{
    /**
     * Cancel the authorization token for a Klarna Payments Quote
     *
     * @param Klarna_Payments_Model_Quote $klarnaQuote
     *
     * @return bool
     */
    public function cancelKlarnaQuoteAuthorizationToken(Klarna_Payments_Model_Quote $klarnaQuote)
    {
        $result = false;
        if ($klarnaQuote->getAuthorizationToken()) {
            $response = Mage::helper('klarna_core')->getPurchaseApiInstance('klarna_direktdebit')
                            ->cancelAuthorization($klarnaQuote->getAuthorizationToken());

            if (!$response->getIsSuccessful()) {
                Mage::logException(
                    new Klarna_Payments_Exception(
                        sprintf(
                            'Unable to cancel authorization token %s for quote #%d',
                            $klarnaQuote->getAuthorizationToken(),
                            $klarnaQuote->getQuoteId()
                        )
                    )
                );
            } else {
                $result = true;
            }

            $klarnaQuote->setAuthorizationToken(null);
            $klarnaQuote->save();
        }

        return $result;
    }

    /**
     * Get Quote object based off current checkout quote
     *
     * @return Klarna_Payments_Model_Quote
     */
    public function getKlarnaQuote()
    {
        if (null === $this->_klarnaQuote) {
            $this->_klarnaQuote = Mage::getModel('klarna_payments/quote')
                                      ->loadActiveByQuote($this->getQuote(), 'klarna_direktdebit');
        }

        return $this->_klarnaQuote;
    }

    /**
     * Get Klarna payments api instance
     *
     * @return Klarna_Payments_Model_Api_Kasper_Purchase
     */
    public function getPurchaseApiInstance()
    {
        return Mage::helper('klarna_core')->getPurchaseApiInstance('klarna_direktdebit');
    }

    /**
     * Create a new klarna quote object
     *
     * @param Varien_Object $klarnaPayments
     *
     * @return Klarna_Payments_Model_Quote
     * @throws Exception
     */
    protected function _createNewKlarnaQuote($klarnaPayments)
    {
        $klarnaQuote = Mage::getModel('klarna_payments/quote');
        $klarnaQuote->setData(
            array(
                'session_id'     => $klarnaPayments->getSessionId(),
                'is_active'      => 1,
                'quote_id'       => $this->getQuote()->getId(),
                'payment_method' => 'klarna_direktdebit'
            )
        );

        if ($klarnaPayments->getClientToken()) {
            $klarnaQuote->setClientToken($klarnaPayments->getClientToken());
        }

        $klarnaQuote->save();

        return $klarnaQuote;
    }
}
