<?php
/**
 * Copyright 2017 Klarna Bank AB (publ)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Core
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * Generate order line details for reward
 */
class Klarna_Core_Model_Api_Builder_Orderline_Reward extends Klarna_Core_Model_Api_Builder_Orderline_Abstract
{
    /**
     * Collect totals process.
     *
     * @param Klarna_Kco_Model_Api_Builder_Abstract $checkout
     *
     * @return $this
     */
    public function collect($checkout)
    {
        /** @var Mage_Sales_Model_Quote $quote */
        $quote  = $checkout->getObject();
        $totals = $quote->getTotals();

        if (is_array($totals) && isset($totals['reward'])) {
            $total  = $totals['reward'];
            $helper = Mage::helper('klarna_core');
            $value  = $helper->toApiFloat($total->getValue());

            $checkout->addData(
                array(
                'reward_unit_price'   => $value,
                'reward_tax_rate'     => 0,
                'reward_total_amount' => $value,
                'reward_tax_amount'   => 0,
                'reward_title'        => $total->getTitle(),
                'reward_reference'    => $total->getCode()

                )
            );
        }

        return $this;
    }

    /**
     * Add grand total information to address
     *
     * @param Klarna_Kco_Model_Api_Builder_Abstract $checkout
     *
     * @return $this
     */
    public function fetch($checkout)
    {
        if ($checkout->getRewardTotalAmount()) {
            $checkout->addOrderLine(
                array(
                'type'             => Klarna_Core_Model_Api_Builder_Orderline_Discount::ITEM_TYPE_DISCOUNT,
                'reference'        => $checkout->getRewardReference(),
                'name'             => $checkout->getRewardTitle(),
                'quantity'         => 1,
                'unit_price'       => $checkout->getRewardUnitPrice(),
                'tax_rate'         => $checkout->getRewardTaxRate(),
                'total_amount'     => $checkout->getRewardTotalAmount(),
                'total_tax_amount' => $checkout->getRewardTaxAmount(),
                )
            );
        }

        return $this;
    }
}
