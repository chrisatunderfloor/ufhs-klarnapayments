<?php
/**
 * Copyright 2016 Klarna AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Core
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * Klarna api integration interface for purchases
 */
interface Klarna_Core_Model_Api_PurchaseApiInterface
{
    /**
     * Create or update a session
     *
     * @param string     $sessionId
     * @param bool|false $createIfNotExists
     * @param bool|false $updateAllowed
     *
     * @return Klarna_Core_Model_Api_Response
     */
    public function initKlarnaSession($sessionId = null, $createIfNotExists = false, $updateAllowed = false);

    /**
     * @return Klarna_Core_Model_Api_Response
     */
    public function createSession();

    /**
     * @param string $sessionId
     *
     * @return Klarna_Core_Model_Api_Response
     */
    public function updateSession($sessionId);

    /**
     * Get Klarna Reservation Id
     *
     * @return string
     */
    public function getReservationId();
}
