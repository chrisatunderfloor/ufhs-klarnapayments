<?php
/**
 * Copyright 2016 Klarna AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Core
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * Klarna order used to associate a Klarna order with a Magento order
 *
 * @method Klarna_Core_Model_Order setKlarnaOrderId()
 * @method int getKlarnaOrderId()
 * @method Klarna_Core_Model_Order setSessionId()
 * @method string getSessionId()
 * @method Klarna_Core_Model_Order setReservationId()
 * @method string getReservationId()
 * @method Klarna_Core_Model_Order setOrderId()
 * @method int getOrderId()
 * @method Klarna_Core_Model_Order setIsAcknowledged(int $value)
 * @method int getIsAcknowledged()
 */
class Klarna_Core_Model_Order extends Mage_Core_Model_Abstract
{
    /**
     * Init
     */
    public function _construct()
    {
        $this->_init('klarna_core/order');
    }

    /**
     * Load by session id
     *
     * @param string $sessionId
     *
     * @return $this
     */
    public function loadBySessionId($sessionId)
    {
        return $this->load($sessionId, 'session_id');
    }

    /**
     * Load by an order
     *
     * @param Mage_Sales_Model_Order $order
     *
     * @return $this
     */
    public function loadByOrder(Mage_Sales_Model_Order $order)
    {
        return $this->load($order->getId(), 'order_id');
    }
}
